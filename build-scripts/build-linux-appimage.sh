#! /bin/bash


set -x
set -e

TEMP_BASE=/tmp
BUILD_DIR=$(mktemp -d -p "$TEMP_BASE" Puck-AppImage-build-XXXXXX)


cleanup () {
    if [ -d "$BUILD_DIR" ]; then
        rm -rf "$BUILD_DIR"
    fi
}

trap cleanup EXIT

# store repo root as variable
REPO_ROOT=$(readlink -f $(dirname $(dirname "$0")))
OLD_CWD=$(readlink -f .)

pushd "$BUILD_DIR"/

cat > AppRun.sh <<\EAT
#! /bin/sh

# make sure to set APPDIR when run directly from the AppDir
if [ -z $APPDIR ]; then APPDIR=$(readlink -f $(dirname "$0")); fi
export LD_LIBRARY_PATH="$APPDIR"/usr/lib
exec "$APPDIR"/usr/bin/puck "$@"

EAT

chmod +x AppRun.sh

# get linuxdeploy and its conda plugin
wget https://github.com/linuxdeploy/linuxdeploy/releases/download/continuous/linuxdeploy-x86_64.AppImage
wget https://raw.githubusercontent.com/TheAssassin/linuxdeploy-plugin-conda/e714783a1ca6fffeeb9dd15bbfce83831bb196f8/linuxdeploy-plugin-conda.sh

# can use the plugin's environment variables to ease some setup
export CONDA_CHANNELS=conda-forge
export CONDA_PACKAGES=xorg-libxi
export PIP_REQUIREMENTS="."

# make sure linuxdeploy-plugin-conda switches to repo root so that the "." pip requirement can be satisfied
export PIP_WORKDIR="$REPO_ROOT"
export PIP_VERBOSE=1

export UPD_INFO="gh-releases-zsync|Puck|Puck|latest|Puck-*x86_64.AppImage"

#mkdir -p AppDir/usr/share/metainfo/
#cp "$REPO_ROOT"/*.appdata.xml AppDir/usr/share/metainfo/

chmod a+x ./linuxdeploy*.{sh,AppImage}
./linuxdeploy-x86_64.AppImage --appdir AppDir --plugin conda -d "$REPO_ROOT"/assets/puck.desktop -i "$REPO_ROOT"/assets/puck.svg --custom-apprun AppRun.sh

# remove unused files from AppDir manually
# these files are nothing the conda plugin could remove manually
rm AppDir/usr/conda/lib/python3.6/site-packages/PyQt5/QtWebEngine* || true
rm -r AppDir/usr/conda/lib/python3.6/site-packages/PyQt5/Qt/translations/qtwebengine* || true
rm AppDir/usr/conda/lib/python3.6/site-packages/PyQt5/Qt/resources/qtwebengine* || true
rm -r AppDir/usr/conda/lib/python3.6/site-packages/PyQt5/Qt/qml/QtWebEngine* || true
rm AppDir/usr/conda/lib/python3.6/site-packages/PyQt5/Qt/plugins/webview/libqtwebview* || true
rm AppDir/usr/conda/lib/python3.6/site-packages/PyQt5/Qt/libexec/QtWebEngineProcess* || true
rm AppDir/usr/conda/lib/python3.6/site-packages/PyQt5/Qt/lib/libQt5WebEngine* || true

ls -al AppDir/

python3.6 "$REPO_ROOT/setup.py" || true

wget https://github.com/AppImage/AppImageKit/releases/download/continuous/appimagetool-x86_64.AppImage
chmod a+x appimagetool*.AppImage
./appimagetool*.AppImage AppDir -u "$UPD_INFO"

# Print version to test if the AppImage runs at all
chmod a+x Puck*.AppImage*
xvfb-run ./Puck*.AppImage* --version

# move AppImage back to old CWD
mv Puck*.AppImage* "$OLD_CWD"/

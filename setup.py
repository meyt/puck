from setuptools import setup, find_packages

setup(
    name="puck",
    description="A hello world python app",
    version='0.1.0',
    author="meyt",
    packages=find_packages(),
    entry_points={
        'console_scripts': [
            'puck = puck:main'
        ]
    }
)

